public class Jackpot {
    public static void main(String[] args) {
        System.out.println("Welcome to jackpot");
        Board board=new Board();
        boolean gameOver=false;
        int numOfTilesClosed=0;
        while (!gameOver){
            System.out.println(board.toString());
            boolean result = board.playATurn();
            if(result){
                gameOver=true;
            }else{
                numOfTilesClosed+=1;
            }
        }
        if(numOfTilesClosed>=7){
            System.out.println("You won");
        }else{
            System.out.println("You lost");
        }
    }
}