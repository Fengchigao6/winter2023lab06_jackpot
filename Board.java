public class Board {
    private Die firstDie;
    private Die secondDie;
    private boolean[] tiles;
    public Board(){
        this.firstDie =new Die();
        this.secondDie=new Die();
        this.tiles = new boolean[12];
    }
    public String toString(){
        String tiles = "";
        for (int i = 0; i < 12; i++) {
            if(this.tiles[i]){
                if(i==11){
                    tiles +="X";
                }else {
                    tiles += "X ";
                }
            }else{
                if(i==11){
                    tiles += i+1;
                }else {
                    tiles += i + 1 + " ";
                }
            }
        }
        return tiles;
    }
   public boolean playATurn(){
        this.firstDie.roll();
        this.secondDie.roll();
        System.out.println("First die: "+this.firstDie.getFaceValue());
        System.out.println("Second die: "+this.secondDie.getFaceValue());
        int sumOfDice = this. firstDie.getFaceValue()+ this.secondDie.getFaceValue();
        if(!this.tiles[sumOfDice-1]){
            this.tiles[sumOfDice-1]=true;
            System.out.println("Closing tile equal to sum: "+sumOfDice);
            return false;
        }else if(!this.tiles[this.firstDie.getFaceValue()-1]){
            this.tiles[this.firstDie.getFaceValue()-1]=true;
            System.out.println("Closing tile with the same value as die one: "+this.firstDie.getFaceValue());
            return false;
        }else if(!this.tiles[this.secondDie.getFaceValue()-1]){
            this.tiles[this.secondDie.getFaceValue()-1]=true;
            System.out.println("Closing tile with the same value as die two: "+this.secondDie.getFaceValue());
            return false;
        }else {
            System.out.println("All the tiles for these values are already shut");
            return true;
        }
    }
}
